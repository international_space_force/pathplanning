from pymavlink import mavutil
from datetime import datetime
import json

connection = mavutil.mavlink_connection('udpin:127.0.0.1:4201')


def incoming_msg():
    while True:
        try:
            data = connection.recv_match().to_dict()
            # print(data)
            lat2 = data['param1']
            lon2 = data['param2']
            with open('gps.log', 'a') as file:
                file.write(str(lat2) + " " + str(lon2) + "\n")
        except:
            pass


print("Listening for messages...")

try:
    while True:
        incoming_msg()
except KeyboardInterrupt:
    print("Stopped by user")
